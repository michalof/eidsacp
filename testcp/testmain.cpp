#include "../eidsacp/registerUser.h"
#include <tchar.h>
#include <Windows.h>
#include <wincred.h>
#include <ntsecapi.h>

#pragma comment (lib, "Credui.lib")

int main(int argc, char** argv)
{
	if (argc == 1)
	{
		CREDUI_INFO ui_info = { sizeof CREDUI_INFO };

		void* cred_blob = nullptr;
		ULONG cred_blob_size = 0;
		ULONG auth_package = 0;
		DWORD ret;
		ret = CredUIPromptForWindowsCredentialsW(&ui_info, 0,
			&auth_package, NULL, 0, &cred_blob, &cred_blob_size, NULL,
			CREDUIWIN_ENUMERATE_CURRENT_USER);

		SecureZeroMemory(cred_blob, cred_blob_size);
		CoTaskMemFree(cred_blob);

	}
	else
	{
		HMODULE setup = LoadLibrary(_T("eidsacp.dll"));
		if (!setup)
		{
			return 1;
		}

		using fct_setUser = void(void);

		fct_setUser * setupUser = (fct_setUser *)GetProcAddress(setup, "setupUser");
		if (setupUser == 0) {
			FreeLibrary(setup);
			return 1;
		}

		setupUser();
		FreeLibrary(setup);
	}
}
