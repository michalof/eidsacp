#include "helper.h"
#include <Shlwapi.h>

HRESULT FieldDescriptorCoAllocCopy(const CREDENTIAL_PROVIDER_FIELD_DESCRIPTOR & rcpfd, CREDENTIAL_PROVIDER_FIELD_DESCRIPTOR ** ppcpfd)
{
	HRESULT hr;
	*ppcpfd = nullptr;
	DWORD cbStruct = sizeof(**ppcpfd);

	CREDENTIAL_PROVIDER_FIELD_DESCRIPTOR *pcpfd = (CREDENTIAL_PROVIDER_FIELD_DESCRIPTOR*)CoTaskMemAlloc(cbStruct);
	if (pcpfd)
	{
		pcpfd->dwFieldID = rcpfd.dwFieldID;
		pcpfd->cpft = rcpfd.cpft;
		pcpfd->guidFieldType = rcpfd.guidFieldType;

		if (rcpfd.pszLabel)
		{
			hr = SHStrDupW(rcpfd.pszLabel, &pcpfd->pszLabel);
		}
		else
		{
			pcpfd->pszLabel = nullptr;
			hr = S_OK;
		}
	}
	else
	{
		hr = E_OUTOFMEMORY;
	}

	if (SUCCEEDED(hr))
	{
		*ppcpfd = pcpfd;
	}
	else
	{
		CoTaskMemFree(pcpfd);
	}

	return hr;
}
