#pragma once


/*#include <boost/algorithm/hex.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/error.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <openssl/sha.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>
#include <array>
#include <openssl/evp.h>
#include <openssl/sha.h>
#include <openssl/crypto.h>
#include <openssl/rand.h>*/
#include <array>
#include <string>
#include <vector>


#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS



//using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
//namespace http = boost::beast::http;    // from <boost/beast/http.hpp>
//namespace ssl = boost::asio::ssl;       // from <boost/asio/ssl.hpp>
//namespace pt = boost::property_tree;

struct Person
{
	//eID.PersonalData.GivenName.
	std::wstring persoData; 
	std::wstring secret; // secondFactorPassword

	std::array<uint8_t, 64> sign() const;

	bool check(const std::array<uint8_t, 64>& signature) const;

	std::array<uint8_t, 32> toHash(const std::array<uint8_t, 32>& _salt) const;

	~Person();
};

std::string getXML();

std::string getXMLwithoutAusweisApp(std::string perso_pin);

std::string getPreXML();

Person xml2person(std::string xml);

enum class card_status
{
	no_device,
	pin_required,
	good
};

card_status find_npa_card();
