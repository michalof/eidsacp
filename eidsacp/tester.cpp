#include <iostream>
#include <vector>
#include <windows.h>
#include "crypto.h"
#include "registerUser.h"
#include "selbstauskunft.h"

using namespace std;

extern "C" void CALLBACK testCP()
{
	//DllRegisterServer();
	DllUnregisterServer();
	registerUser(currentUserSID(), L"root"sv, L"geheim"sv, "654321");

	std::wstring sid = currentUserSID();

	CPUser cpuser(sid);
	vector<uint8_t> cpw;
	array<uint8_t, 16> iv;
	array<uint8_t, 32> salt;
	vector<uint8_t> hash;
	cpuser.getData(cpw, iv, salt, hash);


	wstring accPw;
	wstring_view secFacPw = L"geheim";
	int ret = checkPassword(secFacPw, salt, hash);
	if (ret == 0) {
		accPw = getPassword(sid, secFacPw, "654321");
	}
	else {
		// wrong secondFactorPassword
	}
	SecureZeroMemory(accPw.data(), accPw.size());
}