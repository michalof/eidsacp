#pragma once
#include <array>
#include <string>
#include <string_view>
#include <vector>
#include <Windows.h>

enum cpv : unsigned char { cp_secret, cp_salt, cp_iv, cp_hash };

class CPUser
{
public:
	CPUser(std::wstring_view _sid);
	LSTATUS setData(std::vector<uint8_t>& _accountPassword, std::array<uint8_t, 16>& _iv, std::array<uint8_t, 32>& _salt, std::vector<uint8_t>& _hash);
	LSTATUS getData(std::vector<uint8_t>& _accountPassword, std::array<uint8_t, 16>& _iv, std::array<uint8_t, 32>& _salt, std::vector<uint8_t>& _hash);

private:
	template <typename T> LSTATUS setValue(HKEY _ukey, const cpv _name, const T& _value) const;
	template <unsigned N> LSTATUS getValue(HKEY _ukey, const cpv _name, std::array<uint8_t, N>& _data) const;
	LSTATUS getValue(HKEY _ukey, const cpv _name, std::vector<uint8_t>& _data) const;

	LSTATUS openKey(PHKEY _ukey, REGSAM _rights);

	std::wstring sid_;
	const std::array<const char*, 4> cpv_names_ = { "secret", "salt", "iv", "hash" };
};

class CPRegEditor
{
public:
	CPRegEditor();
	~CPRegEditor();
	std::vector<std::wstring> enumerateRegisteredSIDS() const;

private:
	HKEY cpkey_;
};

std::wstring currentUserSID();

bool registerUser(std::wstring_view _sid, std::wstring_view _accountPassword, std::wstring_view _secondFactorPassword, const std::string& perso_pin);

std::wstring getPassword(std::wstring_view sid, std::wstring_view _secondFactorPassword, const std::string& perso_pin);

// T == vector<uint8_t> or array<uint8_t, n>/*
template <typename T>
LSTATUS CPUser::setValue(HKEY _ukey, const cpv _name, const T& _value) const
{
	return RegSetValueExA(_ukey, cpv_names_[_name], NULL, REG_BINARY, _value.data(), (DWORD)_value.size());
}

template<unsigned N>
LSTATUS CPUser::getValue(HKEY _ukey, const cpv _name, std::array<uint8_t, N>& _data) const
{
	DWORD value_size = N;
	return RegQueryValueExA(_ukey, cpv_names_[_name], NULL, NULL, _data.data(), &value_size); // TODO what if value_size changes?
}

extern "C" void CALLBACK setupUser();

extern "C" HRESULT CALLBACK DllRegisterServer();

extern "C" HRESULT CALLBACK DllUnregisterServer();
