#include "crypto.h"
#include <array>
#include <boost/asio/ssl/error.hpp>
#include <openssl/rand.h>
#include <openssl/evp.h>
#include <vector>
#include <wincred.h>
#include <ntsecapi.h>
#include <credentialprovider.h>

using namespace std;


void handleErrors(void)
{
//	ERR_print_errors_fp(stderr);
//	abort();
}

vector<uint8_t> encrypt(const vector<uint8_t>& plaintext, const array<uint8_t, 32>& key, const array<uint8_t, 16>& iv) {
	EVP_CIPHER_CTX *ctx;
	int len;
	int ciphertext_len = ((plaintext.size() + 16 + 15) & ~15);
	vector<uint8_t> cipher(ciphertext_len);

	/* Create and initialise the context */
	if (!(ctx = EVP_CIPHER_CTX_new())) handleErrors();
	
	/* Initialise the encryption operation. IMPORTANT - ensure you use a key
	 * and IV size appropriate for your cipher
	 * In this example we are using 256 bit AES (i.e. a 256 bit key). The
	 * IV size for *most* modes is the same as the block size. For AES this
	 * is 128 bits */
	if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key.data(), iv.data()))
		handleErrors();
	
	/* Provide the message to be encrypted, and obtain the encrypted output.
	 * EVP_EncryptUpdate can be called multiple times if necessary
	 */
	if (1 != EVP_EncryptUpdate(ctx, cipher.data(), &len, plaintext.data(), (int)plaintext.size()))
		handleErrors();
	ciphertext_len = len;
	
	/* Finalise the encryption. Further ciphertext bytes may be written at
	 * this stage.
	 */
	if (1 != EVP_EncryptFinal_ex(ctx, cipher.data() + len, &len)) handleErrors();
	ciphertext_len += len;
	
	cipher.resize(ciphertext_len);
	
	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);

	return cipher;
}

vector<uint8_t> decrypt(const vector<uint8_t>& ciphertext, const array<uint8_t, 32>& key, const array<uint8_t, 16>& iv) {

	//TODO catch ciphertext/plaintext empty

	EVP_CIPHER_CTX *ctx;
	int len;
	int decryptedtext_len = (int)ciphertext.size();
	vector<uint8_t> decryptedtext(decryptedtext_len);

	/* Create and initialise the context */
	if (!(ctx = EVP_CIPHER_CTX_new())) handleErrors();
	
	/* Initialise the decryption operation. IMPORTANT - ensure you use a key
	 * and IV size appropriate for your cipher
	 * In this example we are using 256 bit AES (i.e. a 256 bit key). The
	 * IV size for *most* modes is the same as the block size. For AES this
	 * is 128 bits */
	if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key.data(), iv.data()))
		handleErrors();
	
	/* Provide the message to be decrypted, and obtain the plaintext output.
	 * EVP_DecryptUpdate can be called multiple times if necessary
	 */
	if (1 != EVP_DecryptUpdate(ctx, decryptedtext.data(), &len, ciphertext.data(), (int)ciphertext.size()))
		handleErrors();
	decryptedtext_len = len;
	
	/* Finalise the decryption. Further plaintext bytes may be written at
	 * this stage.
	 */
	if (1 != EVP_DecryptFinal_ex(ctx, decryptedtext.data() + len, &len)) handleErrors();
	decryptedtext_len += len;
	decryptedtext.resize(decryptedtext_len);
	
	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);

	return decryptedtext;
}

vector<uint8_t> hashPassword(std::wstring_view _pw, const array<uint8_t, 32>& _salt)
{
	vector<uint8_t> hash(64);
	// TODO better cast?
	PKCS5_PBKDF2_HMAC((const char*) _pw.data(), (int)_pw.size(), _salt.data(), (int)_salt.size(), 128000, EVP_sha512(), (int)hash.size(), hash.data());
	return hash;
}

int checkPassword(wstring_view _pw, const array<uint8_t, 32>& _salt, const vector<uint8_t>& _hash)
{
	vector<uint8_t> newHash = hashPassword(_pw, _salt);
	if (newHash == _hash) {
		SecureZeroMemory(newHash.data(), newHash.size());
		return 0;
	}
	SecureZeroMemory(newHash.data(), newHash.size());
	return 1;
}

std::pair<void*, size_t> create_cred_package(std::wstring_view _domain, std::wstring_view _user, std::wstring_view _password, int _type)
{
	const size_t total_size = sizeof(KERB_INTERACTIVE_UNLOCK_LOGON) + (_domain.size() + _user.size() + _password.size()) * sizeof(wchar_t);
	KERB_INTERACTIVE_UNLOCK_LOGON* package = (KERB_INTERACTIVE_UNLOCK_LOGON*)CoTaskMemAlloc(total_size);
	ZeroMemory(package, total_size);

	switch (_type)
	{
	case CPUS_LOGON:
		package->Logon.MessageType = KerbInteractiveLogon;
		break;
	case CPUS_UNLOCK_WORKSTATION:
		package->Logon.MessageType = KerbWorkstationUnlockLogon;
		break;
	default:
		package->Logon.MessageType = KERB_LOGON_SUBMIT_TYPE(0);
		break;
	}

	auto pack_string = [&](size_t& _offset, std::wstring_view _str) {
		const USHORT strByteSize = USHORT(_str.size() * 2);
		std::copy(_str.begin(), _str.end(), PWCHAR((char*)package + _offset));
		UNICODE_STRING ret{ strByteSize, strByteSize, (PWCHAR)_offset };
		_offset += strByteSize;
		return ret;
	};

	size_t offset = sizeof(KERB_INTERACTIVE_LOGON);
	package->Logon.LogonDomainName = pack_string(offset, _domain);
	package->Logon.UserName = pack_string(offset, _user);
	package->Logon.Password = pack_string(offset, _password);
	return { package, total_size };
}

