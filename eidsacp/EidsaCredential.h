#pragma once
#include <Windows.h>
#include <string>
#include <credentialprovider.h>

// TODO Inherit from ICredentialProviderCredentialWithFieldOptions and implement GetFieldOptions to enable the password reveal button and touch keyboard auto-invoke in the password field.

class EidsaCredential : public ICredentialProviderCredential2
{
public:
	EidsaCredential(std::wstring sid, CREDENTIAL_PROVIDER_USAGE_SCENARIO _cpus);
	~EidsaCredential();

	// IUnknown
	ULONG AddRef() override;
	ULONG Release() override;
	HRESULT QueryInterface(REFIID riid, void **ppvObject) override;

	// ICredentialProviderCredential
	HRESULT Advise(/*in*/ ICredentialProviderCredentialEvents* pcpce) override;
	HRESULT UnAdvise() override;

	HRESULT SetSelected(/*out*/ BOOL* pbAutoLogon) override;
	HRESULT SetDeselected() override;

	HRESULT GetFieldState(/*in*/ DWORD dwFieldID, /*out*/ CREDENTIAL_PROVIDER_FIELD_STATE* pcpfs, /*out*/ CREDENTIAL_PROVIDER_FIELD_INTERACTIVE_STATE* pcpfis) override;

	HRESULT GetStringValue(/*in*/ DWORD dwFieldID, /*out, string*/ LPWSTR* ppsz) override;
	HRESULT GetBitmapValue(/*in*/ DWORD dwFieldID, /*out*/ HBITMAP* phbmp) override;
	HRESULT GetCheckboxValue(/*in*/ DWORD dwFieldID, /*out*/ BOOL* pbChecked, /*out, string*/ LPWSTR* ppszLabel) override;
	HRESULT GetSubmitButtonValue(/*in*/ DWORD dwFieldID, /*out*/ DWORD* pdwAdjacentTo) override;

	HRESULT GetComboBoxValueCount(/*in*/ DWORD dwFieldID, /*out*/ DWORD* pcItems, /*out*/ DWORD* pdwSelectedItem) override;
	HRESULT GetComboBoxValueAt(/*in*/ DWORD dwFieldID, DWORD dwItem, /*out, string*/ LPWSTR* ppszItem) override;

	HRESULT SetStringValue(/*in*/ DWORD dwFieldID, /*in, string*/ LPCWSTR psz) override;
	HRESULT SetCheckboxValue(/*in*/ DWORD dwFieldID, /*in*/ BOOL bChecked) override;
	HRESULT SetComboBoxSelectedValue(/*in*/ DWORD dwFieldID, /*in*/ DWORD dwSelectedItem) override;
	HRESULT CommandLinkClicked(/*in*/ DWORD dwFieldID) override;

	HRESULT GetSerialization(/*out*/ CREDENTIAL_PROVIDER_GET_SERIALIZATION_RESPONSE* pcpgsr, /*out*/ CREDENTIAL_PROVIDER_CREDENTIAL_SERIALIZATION* pcpcs, /*out*/ LPWSTR* ppszOptionalStatusText, /*out*/ CREDENTIAL_PROVIDER_STATUS_ICON* pcpsiOptionalStatusIcon) override;
	HRESULT ReportResult(/*in*/ NTSTATUS ntsStatus, /*in*/ NTSTATUS ntsSubstatus, /*out*/ LPWSTR* ppszOptionalStatusText, /*out*/ CREDENTIAL_PROVIDER_STATUS_ICON* pcpsiOptionalStatusIcon) override;

	// ICredentialProviderCredential2
	HRESULT GetUserSid(/*out*/ LPWSTR *sid) override;

protected:
	std::pair<std::wstring, std::wstring> get_domain_and_username() const;

private:
	void secureEraseFields();

	bool display_pin_ = true;

	std::wstring sid_;
	long refCount_ = 1; // modified by IUnknown->AddRef and IUnknown->Release
	ICredentialProviderCredentialEvents * events_ = nullptr; // modified by Advise/UnAdvise
	CREDENTIAL_PROVIDER_USAGE_SCENARIO cpus_ = CPUS_INVALID;
	std::wstring password_;
	std::wstring pin_;
};
