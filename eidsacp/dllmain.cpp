#include "dllmain.h"
#include "guid.h"
#include "EidsaCredentialProvider.h"

HINSTANCE g_hinst = NULL; // handle of this dll
static long g_dllrefs = 0; // global dll reference count

class CClassFactory : IClassFactory
{
public:
	CClassFactory();
	~CClassFactory();

	// IUnknown
	ULONG AddRef() override;
	ULONG Release() override;
	HRESULT QueryInterface(REFIID riid, void **ppvObject) override;

	// IClassFactory
	HRESULT CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppvObject) override;
	HRESULT LockServer(BOOL fLock) override;

private:
	long refCount_ = 1;
};

CClassFactory::CClassFactory()
{
	DllAddRef();
}

CClassFactory::~CClassFactory()
{
	DllRelease();
}

inline ULONG CClassFactory::AddRef()
{
	return ++refCount_;
}

inline ULONG CClassFactory::Release()
{
	long refCount = --refCount_;
	if (!refCount)
	{
		delete this;
	}
	return refCount;
}

inline HRESULT CClassFactory::QueryInterface(REFIID riid, void ** ppvObject)
{
	if (!ppvObject) return E_INVALIDARG;
	*ppvObject = nullptr;

	HRESULT hres = S_OK;

	if (riid == IID_IUnknown)
	{
		*ppvObject = static_cast<IUnknown *>(this);
	}
	else if (riid == IID_IClassFactory)
	{
		*ppvObject = static_cast<IClassFactory *>(this);
	}
	else
	{
		hres = E_NOINTERFACE;
	}

	if (*ppvObject) this->AddRef();
	
	return hres;
}

HRESULT CClassFactory::CreateInstance(IUnknown * pUnkOuter, REFIID riid, void ** ppvObject)
{
	if (!ppvObject) return E_INVALIDARG;
	if (pUnkOuter) return CLASS_E_NOAGGREGATION;

	HRESULT hres = E_NOINTERFACE;

	if (riid == IID_ICredentialProvider)
	{
		EidsaCredentialProvider *cp = new EidsaCredentialProvider();
		if (cp)
		{
			hres = cp->QueryInterface(riid, ppvObject);
			cp->Release();
		}
		else
		{
			hres = E_OUTOFMEMORY;
		}
	}

	return hres;
}

HRESULT CClassFactory::LockServer(BOOL lock)
{
	if (lock) DllAddRef();
	else DllRelease();
	return S_OK;
}

HRESULT DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID * ppvObj)
{
	HRESULT hres = CLASS_E_CLASSNOTAVAILABLE;

	if (rclsid == CLSID_EidsaCredentialProvider)
	{
		CClassFactory *fac = new CClassFactory();
		if (fac)
		{
			*ppvObj = nullptr;
			hres = fac->QueryInterface(riid, ppvObj);
			fac->Release();
		}
		else
		{
			hres = E_OUTOFMEMORY;
		}
	}

	return hres;
}

HRESULT DllCanUnloadNow()
{
	return (g_dllrefs > 0) ? S_FALSE : S_OK;
}

void DllAddRef()
{
	InterlockedIncrement(&g_dllrefs);
}

void DllRelease()
{
	InterlockedDecrement(&g_dllrefs);
}

STDAPI_(BOOL) DllMain(__in HINSTANCE hinstDll, __in DWORD dwReason, __in void *)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		DisableThreadLibraryCalls(hinstDll);
		g_hinst = hinstDll;
		break;
	case DLL_PROCESS_DETACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
}
