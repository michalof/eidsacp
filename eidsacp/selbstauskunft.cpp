#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS

#include <array>
#include <boost/algorithm/hex.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <cstdlib>
#include <iostream>
#include <openssl/conf.h>
#include <openssl/crypto.h>
#include <openssl/sha.h>
#include <sstream>
#include <string>
#include <tchar.h>
#include <winscard.h>

#include "eidclient.h"
#include "selbstauskunft.h"
#include "dllmain.h"

#ifndef CM_IOCTL_GET_FEATURE_REQUEST
#define CM_IOCTL_GET_FEATURE_REQUEST SCARD_CTL_CODE(3400)
#endif
#ifndef FEATURE_EXECUTE_PACE
#define FEATURE_EXECUTE_PACE 0x20
#endif
#ifndef PCSC_TLV_ELEMENT_SIZE
#define PCSC_TLV_ELEMENT_SIZE (1+1+4)
#endif

#pragma comment (lib, "Winscard.lib")

using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
namespace http = boost::beast::http;    // from <boost/beast/http.hpp>
namespace ssl = boost::asio::ssl;       // from <boost/asio/ssl.hpp>
namespace pt = boost::property_tree;

using namespace std;

// class member functions

Person::~Person() {
	SecureZeroMemory(persoData.data(), persoData.size());
	SecureZeroMemory(secret.data(), secret.size());
}

array<uint8_t, 64> Person::sign() const
{
	array<uint8_t, 32> salt = {};
	RAND_bytes(salt.data(), (int) salt.size());
	array<uint8_t, 32> hash = toHash(salt);
	array<uint8_t, 64> sign;
	copy(salt.begin(), salt.end(), sign.begin());
	copy(hash.begin(), hash.end(), sign.begin() + salt.size());
	return sign;
}

bool Person::check(const array<uint8_t, 64>& signature) const
{
	array<uint8_t, 32> salt;
	array<uint8_t, 32> hash;
	copy(signature.begin(), signature.begin() + salt.size(), salt.begin());
	copy(signature.begin() + salt.size(), signature.end() , hash.begin());
	return hash == toHash(salt);
}

array<uint8_t, 32> Person::toHash(const array<uint8_t, 32>& _salt) const
{
	wstring data = persoData + L'\0' + secret;

	array<uint8_t, 32> hash;
	PKCS5_PBKDF2_HMAC((char*)data.data(), (int)data.length()*2, _salt.data(), (int) _salt.size(), 128000, EVP_sha512(), (int) hash.size(), hash.data());
	return hash;
}

// end class member functions 

void getResponseFromRequest(/*in*/tcp::socket& socket, /*in*/tcp::resolver& resolver, /*out*/http::response<http::empty_body>& result) {
	auto endpoints = resolver.resolve("127.0.0.1", "24727");
	boost::asio::connect(socket, endpoints.begin(), endpoints.end());
	http::request<http::string_body> req{ http::verb::get, "/eID-Client?tcTokenURL=https://www.autentapp.de/AusweisAuskunft/WebServiceRequesterServlet?mode=xml", 11 };
	req.set(http::field::host, "127.0.0.1");
	http::write(socket, req);
	boost::beast::flat_buffer buffer;

	do
	{
		http::read(socket, buffer, result);
		std::cout << result << std::endl;	// TODO why this? not needed?
	} while (result.result() == http::status::processing);

	if (result.result() != http::status::see_other)
	{
		std::cerr << "Error: No redirect." << std::endl;
		exit(1);
	}
	return;
}

void getRedirectAndHost(/*in*/http::response<http::empty_body>& result, /*out*/boost::string_view& loc, /*out*/boost::string_view& host) {
	auto rloc = result.find("location");
	if (rloc == result.end())
	{
		std::cerr << "Error: No redirect location." << std::endl;
		exit(1);
	}

	loc = rloc->value();

	if (loc.substr(0, 8) != "https://")
	{
		std::cerr << "Error: No https redirect." << std::endl;
		exit(1);
	}

	loc.remove_prefix(8);
	host = loc.substr(0, loc.find('/', 8));
	loc.remove_prefix(host.size());
	return;
}

std::string getSslResponse(/*in*/boost::asio::io_context& ioc, /*in*/tcp::resolver& resolver, /*in*/boost::string_view& loc, /*in*/boost::string_view& host) {
	ssl::context ctx{ ssl::context::sslv23_client };
	ctx.set_verify_mode(ssl::verify_none); //TODO certificate pinning

	ssl::stream<tcp::socket> stream{ ioc, ctx };

	auto endpoints = resolver.resolve(host.to_string(), "https");
	boost::asio::connect(stream.next_layer(), endpoints.begin(), endpoints.end());

	stream.handshake(ssl::stream_base::client);

	http::request<http::string_body> sreq{ http::verb::get, loc, 11 };
	sreq.set(http::field::host, host);

	http::write(stream, sreq);
	http::response<http::string_body> sres;

	boost::beast::flat_buffer buffer;
	http::read(stream, buffer, sres);
	// TODO 	SecureZeroMemory();
	return sres.body();
}

std::string getXML()
{
	/*
	Aufruf https://www.autentapp.de/AusweisAuskunft/WebServiceRequesterServlet?mode=xml
	Redirect
	SSL-Tunnel aufbauen
	Optional: Certificate Pinning (Check Tunnel)
	XML zurueckgeben
	*/

	boost::asio::io_context ioc;
	tcp::resolver resolver{ ioc };
	tcp::socket socket{ ioc };
	auto endpoints = resolver.resolve("127.0.0.1", "24727");

	boost::beast::flat_buffer buffer;
	http::response<http::empty_body> res;
	boost::string_view loc;
	boost::string_view host;

	getResponseFromRequest(socket, resolver, res);

	getRedirectAndHost(res, loc, host);

	return getSslResponse(ioc, resolver, loc, host);
}

std::string getXMLwithoutAusweisApp(std::string perso_pin) {

	string buf;

	wstring eidclient_path(MAX_PATH, 0);
	eidclient_path.resize(GetModuleFileNameW(g_hinst, eidclient_path.data(), MAX_PATH));
	eidclient_path = eidclient_path.substr(0, eidclient_path.find_last_of(L'\\'));
	eidclient_path += L"\\eidclient.dll";

	HMODULE eidsacp = LoadLibraryW(eidclient_path.c_str());
	if (!eidsacp)
	{
		return buf;
	}
	eid_client_t eid_client = (eid_client_t)GetProcAddress(eidsacp, "eid_client");
	if (eid_client == 0) {
		FreeLibrary(eidsacp);
		return buf;
	}
	buf.resize(10240, 3);
	size_t len = buf.size();
	int ret = eid_client(perso_pin.data(), "https://www.autentapp.de/AusweisAuskunft/WebServiceRequesterServlet?mode=xml", buf.data(), &len);
	if (ret != 0) {
		buf.clear();
		FreeLibrary(eidsacp);
		return buf;
	}
	buf.resize(len);
	FreeLibrary(eidsacp);
	return buf;
}


std::string getPreXML()
{
	return R"(<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<eID xmlns="http://bsi.bund.de/eID/" xmlns:ns5="http://www.w3.org/2000/09/xmldsig#" xmlns:ns2="urn:oasis:names:tc:dss:1.0:core:schema" xmlns:ns4="http://www.w3.org/2001/04/xmlenc#" xmlns:ns3="urn:oasis:names:tc:SAML:2.0:assertion">
    <PersonalData>
        <DocumentType>ID</DocumentType>
        <IssuingState>D</IssuingState>
        <GivenNames>MAX</GivenNames>
        <FamilyNames>MUSTERMANN</FamilyNames>
        <ArtisticName></ArtisticName>
        <AcademicTitle></AcademicTitle>
        <DateOfBirth>
            <DateString>19860608</DateString>
            <DateValue>1986-06-08+02:00</DateValue>
        </DateOfBirth>
        <PlaceOfBirth>
            <FreetextPlace>BERLIN</FreetextPlace>
        </PlaceOfBirth>
        <BirthName></BirthName>
        <PlaceOfResidence>
            <StructuredPlace>
                <Street>Rudower Chaussee 25</Street>
                <City>BERLIN</City>
                <State></State>
                <Country>D</Country>
                <ZipCode>12489</ZipCode>
            </StructuredPlace>
        </PlaceOfResidence>
    </PersonalData>
    <OperationsAllowedByUser>
        <DocumentType>ALLOWED</DocumentType>
        <IssuingState>ALLOWED</IssuingState>
        <DateOfExpiry>PROHIBITED</DateOfExpiry>
        <GivenNames>ALLOWED</GivenNames>
        <FamilyNames>ALLOWED</FamilyNames>
        <ArtisticName>ALLOWED</ArtisticName>
        <AcademicTitle>ALLOWED</AcademicTitle>
        <DateOfBirth>ALLOWED</DateOfBirth>
        <PlaceOfBirth>ALLOWED</PlaceOfBirth>
        <Nationality>NOTONCHIP</Nationality>
        <BirthName>ALLOWED</BirthName>
        <PlaceOfResidence>ALLOWED</PlaceOfResidence>
        <ResidencePermitI>NOTONCHIP</ResidencePermitI>
        <RestrictedID>PROHIBITED</RestrictedID>
        <AgeVerification>PROHIBITED</AgeVerification>
        <PlaceVerification>PROHIBITED</PlaceVerification>
    </OperationsAllowedByUser>
    <ns2:Result>
        <ns2:ResultMajor>http://www.bsi.bund.de/ecard/api/1.1/resultmajor#ok</ns2:ResultMajor>
        <ns2:ResultMessage xml:lang="en"></ns2:ResultMessage>
    </ns2:Result>
</eID>)";
}

wstring getTree(pt::wptree tree) {
	wstring str;
	for(auto& v : tree) {
		if (v.second.empty()) {
			str += v.second.get<wstring>(L"") + L"\0"s;
		}
		else {
			str += getTree(v.second);
		}
	}
	return str;
}

Person xml2person(string xml)
{	
	Person p;
	if (!xml.empty())
	{
		wstring xml_data(xml.size(), 0);
		int xml_len = MultiByteToWideChar(CP_UTF8, 0, xml.c_str(), (int)xml.size(), xml_data.data(), (int)xml_data.size());
		xml_data.resize(xml_len);

		pt::wptree tree;
		wstringstream xmlstream(xml_data);
		SecureZeroMemory(xml.data(), xml.size());
		pt::read_xml(xmlstream, tree);
		// TODO      SecureZeroMemory() for xmlstream;

		p.persoData = getTree(tree.get_child(L"eID.PersonalData"));
		// TODO 	SecureZeroMemory() for tree;
	}
	return p;
}

static card_status is_npa_inserted(SCARDCONTEXT _ctx, const char* _reader_name)
{
	card_status status = card_status::no_device;
	LONG ret;
	DWORD protocol;
	SCARDHANDLE card;
	ret = SCardConnectA(_ctx, _reader_name, SCARD_SHARE_EXCLUSIVE, SCARD_PROTOCOL_Tx, &card, &protocol);
	if (ret != 0)
		return status;
	status = card_status::pin_required;

	BYTE recvbuf[1024];
	DWORD recvlen = sizeof(recvbuf);
	ret = SCardControl(card, CM_IOCTL_GET_FEATURE_REQUEST, NULL, 0, recvbuf, sizeof(recvbuf), &recvlen);
	if(ret != 0)
		return status;

	for (size_t i = 0; i + PCSC_TLV_ELEMENT_SIZE <= recvlen; i += PCSC_TLV_ELEMENT_SIZE)
		if (recvbuf[i] == FEATURE_EXECUTE_PACE)
		{
			status = card_status::good;
			break;
		}

	SCardDisconnect(card, SCARD_LEAVE_CARD);
	return status;
}

card_status find_npa_card()
{
	LONG ret;
	SCARDCONTEXT ctx;
	ret = SCardEstablishContext(SCARD_SCOPE_SYSTEM, 0, 0, &ctx);

	char reader_name[128];
	DWORD reader_name_len = sizeof(reader_name);
	ret = SCardListReadersA(ctx, NULL, reader_name, &reader_name_len);

	card_status status = card_status::no_device;
	size_t idx = 0;
	while (idx < reader_name_len)
	{
		card_status reader_status = is_npa_inserted(ctx, reader_name + idx);

		if (reader_status > status)
			status = reader_status;
		idx += strlen(reader_name + idx) + 1;
	}

	SCardReleaseContext(ctx);
	return status;
}
