#pragma once
#include <Windows.h>
#include <credentialprovider.h>

//makes a copy of a field descriptor using CoTaskMemAlloc
HRESULT FieldDescriptorCoAllocCopy(const CREDENTIAL_PROVIDER_FIELD_DESCRIPTOR &rcpfd, CREDENTIAL_PROVIDER_FIELD_DESCRIPTOR **ppcpfd);
