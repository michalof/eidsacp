#pragma once

#include <array>
#include <vector>



std::vector<uint8_t> encrypt(const std::vector<uint8_t>& plaintext, const std::array<uint8_t, 32>& key, const std::array<uint8_t, 16>& iv);

std::vector<uint8_t> decrypt(const std::vector<uint8_t>& ciphertext, const std::array<uint8_t, 32>& key, const std::array<uint8_t, 16>& iv);

std::vector<uint8_t> hashPassword(std::wstring_view _pw, const std::array<uint8_t, 32>& _salt);

int checkPassword(std::wstring_view _pw, const std::array<uint8_t, 32>& _salt, const std::vector<uint8_t>& _hash);

std::pair<void*, size_t> create_cred_package(std::wstring_view _domain, std::wstring_view _user, std::wstring_view _password, int _type);
