#include "crypto.h"
#include "EidsaCredential.h"
#include "EidsaCredentialFields.h"
#include "dllmain.h"
#include "guid.h"
#include "registerUser.h"
#include "resource.h"
#include "selbstauskunft.h"

#include <algorithm>
#include <array>
#include <cctype>
#include <Shlwapi.h>
#include <string>
#include <vector>

#include <sddl.h>
#include <wincred.h>
#include <ntsecapi.h>

#pragma comment (lib, "Shlwapi.lib")

EidsaCredential::EidsaCredential(std::wstring sid, CREDENTIAL_PROVIDER_USAGE_SCENARIO _cpus) :
	sid_(std::move(sid)),
	cpus_(_cpus)
{
	DllAddRef();
}

EidsaCredential::~EidsaCredential()
{
	DllRelease();
}

ULONG EidsaCredential::AddRef()
{
	return ++refCount_;
}

ULONG EidsaCredential::Release()
{
	long refCount = --refCount_;
	if (!refCount)
	{
		delete this;
	}
	return refCount;
}

HRESULT EidsaCredential::QueryInterface(REFIID riid, void ** ppvObject)
{
	if (!ppvObject) return E_INVALIDARG;
	*ppvObject = nullptr;

	HRESULT hres = S_OK;

	if (riid == IID_IUnknown)
	{
		*ppvObject = static_cast<IUnknown *>(static_cast<ICredentialProviderCredential *>(this));
	}
	else if (riid == IID_ICredentialProviderCredential)
	{
		*ppvObject = static_cast<ICredentialProviderCredential *>(this);
	}
	else if (riid == IID_ICredentialProviderCredential2)
	{
		*ppvObject = static_cast<ICredentialProviderCredential2 *>(this);
	}
	else
	{
		hres = E_NOINTERFACE;
	}

	if (*ppvObject) this->AddRef();

	return hres;
}

HRESULT EidsaCredential::Advise(ICredentialProviderCredentialEvents * pcpce)
{
	if (events_ != nullptr) events_->Release();
	return pcpce->QueryInterface(IID_ICredentialProviderCredentialEvents, (void**) &events_);
}

HRESULT EidsaCredential::UnAdvise()
{
	if (events_ != nullptr)
	{
		events_->Release();
		events_ = nullptr;
	}
	return S_OK;
}

HRESULT EidsaCredential::SetSelected(BOOL * pbAutoLogon)
{
	*pbAutoLogon = FALSE;

	display_pin_ = find_npa_card() != card_status::good;
	events_->SetFieldState(this, ECFI_PIN, display_pin_ ? CPFS_DISPLAY_IN_SELECTED_TILE : CPFS_HIDDEN);
	events_->SetFieldSubmitButton(this, ECFI_SUBMIT, display_pin_ ? ECFI_PIN : ECFI_PASSWORD);
	return S_OK;
}

HRESULT EidsaCredential::SetDeselected()
{
	secureEraseFields();
	return S_OK;
}

HRESULT EidsaCredential::GetFieldState(DWORD dwFieldID, CREDENTIAL_PROVIDER_FIELD_STATE * pcpfs, CREDENTIAL_PROVIDER_FIELD_INTERACTIVE_STATE * pcpfis)
{
	if (dwFieldID >= ECFI_NUM_FIELDS) return E_INVALIDARG;

	*pcpfs = g_fields[dwFieldID].display;
	*pcpfis = g_fields[dwFieldID].istate;

	if (dwFieldID == ECFI_PIN)
	{
		if (!display_pin_)
		{
			*pcpfs = CPFS_HIDDEN;
		}
	}
//	else if (dwFieldID == ECFI_PASSWORD)
//	{
//		*pcpfs = CPFS_HIDDEN;
//	}

	return S_OK;
}

HRESULT EidsaCredential::GetStringValue(DWORD dwFieldID, LPWSTR * ppsz)
{
	if (!ppsz) return E_INVALIDARG;
	*ppsz = nullptr;

	HRESULT hres;

	switch (dwFieldID)
	{
	case ECFI_PASSWORD:
		hres = SHStrDupW(password_.c_str(), ppsz);
		return hres;

	case ECFI_PIN:
		hres = SHStrDupW(pin_.c_str(), ppsz);
		return hres;

	case ECFI_LABEL:
		hres = SHStrDupW(g_fields[ECFI_LABEL].descriptor.pszLabel, ppsz);
		return hres;

	default:
		return E_INVALIDARG;
	}
}

HRESULT EidsaCredential::GetBitmapValue(DWORD dwFieldID, HBITMAP * phbmp)
{
	if (ECFI_TILEIMAGE != dwFieldID) return E_INVALIDARG;

	HRESULT hres;
	*phbmp = nullptr;

	HBITMAP hbmp = LoadBitmap(g_hinst, MAKEINTRESOURCE(IDB_BITMAP1));
	if (hbmp)
	{
		hres = S_OK;
		*phbmp = hbmp;
	}
	else
	{
		hres = HRESULT_FROM_WIN32(GetLastError());
	}

	return hres;
}

HRESULT EidsaCredential::GetCheckboxValue(DWORD dwFieldID, BOOL * pbChecked, LPWSTR * ppszLabel)
{
	return E_NOTIMPL;
}

HRESULT EidsaCredential::GetSubmitButtonValue(DWORD dwFieldID, DWORD * pdwAdjacentTo)
{
	// TODO
	if (dwFieldID != ECFI_SUBMIT) return E_INVALIDARG;

	*pdwAdjacentTo = display_pin_ ? ECFI_PIN : ECFI_PASSWORD;
	return S_OK;
}

HRESULT EidsaCredential::GetComboBoxValueCount(DWORD dwFieldID, DWORD * pcItems, DWORD * pdwSelectedItem)
{
	return E_NOTIMPL;
}

HRESULT EidsaCredential::GetComboBoxValueAt(DWORD dwFieldID, DWORD dwItem, LPWSTR * ppszItem)
{
	return E_NOTIMPL;
}

HRESULT EidsaCredential::SetStringValue(DWORD dwFieldID, LPCWSTR psz)
{
	if (!psz) return E_INVALIDARG;

	switch (dwFieldID)
	{
	case ECFI_PASSWORD:
		password_ = psz;
		break;

	case ECFI_PIN:
		pin_.clear();
		std::copy_if(psz, psz + wcslen(psz), std::back_inserter(pin_), isdigit);
		if(pin_.size() > 6) pin_.resize(6);
		events_->SetFieldString(this, ECFI_PIN, pin_.c_str());
		break;

	default:
		return E_INVALIDARG;
	}

	return S_OK;
}

HRESULT EidsaCredential::SetCheckboxValue(DWORD dwFieldID, BOOL bChecked)
{
	return E_NOTIMPL;
}

HRESULT EidsaCredential::SetComboBoxSelectedValue(DWORD dwFieldID, DWORD dwSelectedItem)
{
	return E_NOTIMPL;
}

HRESULT EidsaCredential::CommandLinkClicked(DWORD dwFieldID)
{
	return E_NOTIMPL;
}

HRESULT EidsaCredential::GetSerialization(CREDENTIAL_PROVIDER_GET_SERIALIZATION_RESPONSE * pcpgsr, CREDENTIAL_PROVIDER_CREDENTIAL_SERIALIZATION * pcpcs, LPWSTR * ppszOptionalStatusText, CREDENTIAL_PROVIDER_STATUS_ICON * pcpsiOptionalStatusIcon)
{
	std::string pin;
	std::transform(pin_.begin(), pin_.end(), std::back_inserter(pin), [](wchar_t _num) { return (char)_num; });

	std::wstring accPw = getPassword(sid_, password_, pin);
	if (!accPw.empty()) 
	{
		auto[domain, username] = get_domain_and_username();
		auto[package, package_size] = create_cred_package(domain, username, accPw, cpus_);

		pcpcs->ulAuthenticationPackage = 0;
		pcpcs->clsidCredentialProvider = CLSID_EidsaCredentialProvider;
		pcpcs->rgbSerialization = (BYTE*)package;
		pcpcs->cbSerialization = (ULONG)package_size;

		*pcpgsr = CPGSR_RETURN_CREDENTIAL_FINISHED;
		return S_OK;
	}
	return E_FAIL;
}

HRESULT EidsaCredential::ReportResult(NTSTATUS ntsStatus, NTSTATUS ntsSubstatus, LPWSTR * ppszOptionalStatusText, CREDENTIAL_PROVIDER_STATUS_ICON * pcpsiOptionalStatusIcon)
{
	// No customized icon/text in case of logon failure
	*ppszOptionalStatusText = nullptr;
	*pcpsiOptionalStatusIcon = CPSI_NONE;

	if (FAILED(HRESULT_FROM_NT(ntsStatus)) && events_)
	{
		secureEraseFields();
	}

	return S_OK;
}

HRESULT EidsaCredential::GetUserSid(LPWSTR * sid)
{
	HRESULT hres = E_UNEXPECTED;
	*sid = nullptr;

	if (!sid_.empty())
	{
		hres = SHStrDupW(sid_.c_str(), sid);
	}

	return hres;
}

std::pair<std::wstring, std::wstring> EidsaCredential::get_domain_and_username() const
{
	PSID sid;
	if(!ConvertStringSidToSidW(sid_.c_str(), &sid))
		return std::pair<std::wstring, std::wstring>();

	DWORD name_len = 0;
	DWORD domain_len = 0;
	SID_NAME_USE use;
	if (!LookupAccountSidW(nullptr, sid, nullptr, &name_len, nullptr, &domain_len, &use) &&
		GetLastError() != ERROR_INSUFFICIENT_BUFFER)
	{
		LocalFree(sid);
		return std::pair<std::wstring, std::wstring>();
	}

	std::wstring username(name_len, 0);
	std::wstring domain(domain_len, 0);
	if (!LookupAccountSidW(nullptr, sid, username.data(), &name_len, domain.data(), &domain_len, &use))
	{
		LocalFree(sid);
		return std::pair<std::wstring, std::wstring>();
	}
	username.resize(name_len);
	domain.resize(domain_len);

	LocalFree(sid);
	return { std::move(domain), std::move(username) };
}

void EidsaCredential::secureEraseFields()
{
	if (events_)
	{
		if (!password_.empty())
		{
			SecureZeroMemory(password_.data(), 2 * password_.size());
			password_.erase();
			events_->SetFieldString(this, ECFI_PASSWORD, (LPCWSTR)L"");
		}

		if (!pin_.empty())
		{
			SecureZeroMemory(pin_.data(), 2 * pin_.size());
			pin_.erase();
			events_->SetFieldString(this, ECFI_PIN, (LPCWSTR)L"");
		}
	}
}
