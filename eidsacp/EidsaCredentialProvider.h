#pragma once
#include "EidsaCredential.h"
#include <credentialprovider.h>
#include <vector>
#include <Windows.h>

class EidsaCredentialProvider : public ICredentialProvider, public ICredentialProviderSetUserArray
{
public:
	EidsaCredentialProvider();
	~EidsaCredentialProvider();

	// IUnknown
	ULONG AddRef() override;
	ULONG Release() override;
	HRESULT QueryInterface(REFIID riid, void **ppvObject) override;

	// ICredentialProvider
	HRESULT SetUsageScenario(/*in*/ CREDENTIAL_PROVIDER_USAGE_SCENARIO cpus, /*in*/ DWORD dwFlags) override;
	HRESULT SetSerialization(/*in*/ const CREDENTIAL_PROVIDER_CREDENTIAL_SERIALIZATION* pcpcs) override;

	HRESULT Advise(/*in*/ ICredentialProviderEvents* pcpe, /*in*/ UINT_PTR upAdviseContext) override;
	HRESULT UnAdvise() override;

	HRESULT GetFieldDescriptorCount(/*out*/ DWORD* pdwCount) override;
	HRESULT GetFieldDescriptorAt(/*in*/ DWORD dwIndex, /*out*/ CREDENTIAL_PROVIDER_FIELD_DESCRIPTOR** ppcpfd) override;

	HRESULT GetCredentialCount(/*out*/ DWORD* pdwCount, /*out*/ DWORD* pdwDefault, /*out*/ BOOL* pbAutoLogonWithDefault) override;
	HRESULT GetCredentialAt(/*in*/ DWORD dwIndex, /*out*/ ICredentialProviderCredential** ppcpc) override;

	// ICredentialProviderSetUserArray
	HRESULT SetUserArray(ICredentialProviderUserArray *users) override;

private:
	void setupCredentials();

	std::vector<EidsaCredential *> credentials_;
	long refCount_ = 1; // modified by IUnknown->AddRef and IUnknown->Release
	CREDENTIAL_PROVIDER_USAGE_SCENARIO cpus_ = CPUS_INVALID; // modified by ICredentialProvider->SetUsageScenario
	ICredentialProviderUserArray * users_ = nullptr; // modified by ICredentialProviderSetUserArray->SetUserArray
};

inline ULONG EidsaCredentialProvider::AddRef()
{
	return ++refCount_;
}

inline ULONG EidsaCredentialProvider::Release()
{
	long refCount = --refCount_;
	if (!refCount)
	{
		delete this;
	}
	return refCount;
}

inline HRESULT EidsaCredentialProvider::QueryInterface(REFIID riid, void ** ppvObject)
{
	if (!ppvObject) return E_INVALIDARG;
	*ppvObject = nullptr;

	HRESULT hres = S_OK;

	if (riid == IID_IUnknown)
	{
		*ppvObject = static_cast<IUnknown *>(static_cast<ICredentialProvider *>(this));
	}
	else if (riid == IID_ICredentialProvider)
	{
		*ppvObject = static_cast<ICredentialProvider *>(this);
	}
	else if (riid == IID_ICredentialProviderSetUserArray)
	{
		*ppvObject = static_cast<ICredentialProviderSetUserArray *>(this);
	}
	else
	{
		hres = E_NOINTERFACE;
	}

	if (*ppvObject) this->AddRef();

	return hres;
}
