#include "crypto.h"
#include "registerUser.h"
#include "selbstauskunft.h"
#include <Dpapi.h>
#include <iostream>
#include <openssl/rand.h>
#include <sddl.h> // ConvertSidToStringSidA
#include <strsafe.h>
#include <wincred.h>
#include <ntsecapi.h>
#include "resource.h"

#pragma comment (lib, "Credui.lib")
#pragma comment (lib, "Secur32.lib")
#pragma comment (lib, "Crypt32.lib")

extern HINSTANCE g_hinst;
using namespace std;

wstring currentUserSID()
{
	// Get handle of current process
	HANDLE cur_token_handle;
	/*bool token_ok =*/ OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &cur_token_handle);

	// Get user information from process token
	char tu_buffer[256] = {};
	DWORD ret_len = 0;
	/*bool info_ok =*/ GetTokenInformation(cur_token_handle, TokenUser, tu_buffer, sizeof(tu_buffer), &ret_len);
	TOKEN_USER* token_user = (TOKEN_USER*)tu_buffer;

	// Get SID from user information
	LPWSTR str_sid;
	ConvertSidToStringSidW(token_user->User.Sid, &str_sid);
	wstring sid = str_sid;

	// Do not cause memory leaks
	CloseHandle(cur_token_handle);
	LocalFree(str_sid);

	return sid;
}

CPUser::CPUser(std::wstring_view _sid)
{
	sid_ = _sid;
}

LSTATUS CPUser::setData(std::vector<uint8_t>& _accountPassword, std::array<uint8_t, 16>& _iv, std::array<uint8_t, 32>& _salt, std::vector<uint8_t>& _hash)
{
	HKEY ukey;
	LSTATUS result = openKey(&ukey, KEY_ALL_ACCESS);
	if (result != ERROR_SUCCESS) return result;

	DATA_BLOB salt{ (DWORD)_salt.size(), (BYTE*)_salt.data() };
	DATA_BLOB account_pw{ (DWORD)_accountPassword.size(), (BYTE*)_accountPassword.data() };
	DATA_BLOB account_pw_secret{};

	BOOL res = CryptProtectData(&account_pw, nullptr, &salt, nullptr, nullptr, CRYPTPROTECT_LOCAL_MACHINE, &account_pw_secret);
	SecureZeroMemory(account_pw.pbData, account_pw.cbData);
	if (!res)
	{
		RegCloseKey(ukey);
		return GetLastError();
	}
	_accountPassword.assign(account_pw_secret.pbData, account_pw_secret.pbData + account_pw_secret.cbData);
	SecureZeroMemory(account_pw_secret.pbData, account_pw_secret.cbData);
	LocalFree(account_pw_secret.pbData);

	if (!_hash.empty())
	{
		DATA_BLOB hash{ (DWORD)_hash.size(), (BYTE*)_hash.data() };
		DATA_BLOB hash_secret{};

		res = CryptProtectData(&hash, nullptr, &salt, nullptr, nullptr, CRYPTPROTECT_LOCAL_MACHINE, &hash_secret);
		SecureZeroMemory(hash.pbData, hash.cbData);
		if (!res)
		{
			RegCloseKey(ukey);
			return GetLastError();
		}
		_hash.assign(hash_secret.pbData, hash_secret.pbData + hash_secret.cbData);
		SecureZeroMemory(hash_secret.pbData, hash_secret.cbData);
		LocalFree(hash_secret.pbData);

		setValue(ukey, cp_hash, _hash);
	}

	setValue(ukey, cp_secret, _accountPassword);
	setValue(ukey, cp_iv, _iv);
	setValue(ukey, cp_salt, _salt);

	RegCloseKey(ukey);
	return ERROR_SUCCESS;
}

LSTATUS CPUser::getData(std::vector<uint8_t>& _accountPassword, std::array<uint8_t, 16>& _iv, std::array<uint8_t, 32>& _salt, std::vector<uint8_t>& _hash)
{
	HKEY ukey;
	LSTATUS result = openKey(&ukey, KEY_READ);
	if (result != ERROR_SUCCESS) return result;

	getValue(ukey, cp_secret, _accountPassword);
	getValue(ukey, cp_iv, _iv);
	getValue(ukey, cp_salt, _salt);
	getValue(ukey, cp_hash, _hash);

	DATA_BLOB salt{ (DWORD)_salt.size(), (BYTE*)_salt.data() };
	DATA_BLOB account_pw_secret{ (DWORD)_accountPassword.size(), (BYTE*)_accountPassword.data() };
	DATA_BLOB account_pw{};

	if (CryptUnprotectData(&account_pw_secret, nullptr, &salt, nullptr, nullptr, CRYPTPROTECT_LOCAL_MACHINE, &account_pw))
	{
		string acc_pw_str((char*)account_pw.pbData, account_pw.cbData);		// TODO better conversion?
		_accountPassword = vector<uint8_t>(acc_pw_str.begin(), acc_pw_str.end());
		SecureZeroMemory(acc_pw_str.data(), acc_pw_str.size());

		SecureZeroMemory(account_pw_secret.pbData, account_pw_secret.cbData);
		SecureZeroMemory(account_pw.pbData, account_pw.cbData);
		LocalFree(account_pw.pbData);
	}
	else
	{
		SecureZeroMemory(account_pw_secret.pbData, account_pw_secret.cbData);
		RegCloseKey(ukey);
		return GetLastError();
	}

	DATA_BLOB hash_secret{ (DWORD)_hash.size(), (BYTE*)_hash.data() };
	DATA_BLOB hash{};
	if (CryptUnprotectData(&hash_secret, nullptr, &salt, nullptr, nullptr, CRYPTPROTECT_LOCAL_MACHINE, &hash))
	{
		string hash_str((char*)hash.pbData, hash.cbData);		// TODO better conversion?
		_hash = vector<uint8_t>(hash_str.begin(), hash_str.end());
		SecureZeroMemory(hash_str.data(), hash_str.size());

		SecureZeroMemory(hash_secret.pbData, hash_secret.cbData);
		SecureZeroMemory(hash.pbData, hash.cbData);
		LocalFree(hash.pbData);
	}
	else
	{
		SecureZeroMemory(hash_secret.pbData, hash_secret.cbData);
		RegCloseKey(ukey);
		return GetLastError();
	}

	RegCloseKey(ukey);
	return ERROR_SUCCESS;
}

LSTATUS CPUser::getValue(HKEY _ukey, const cpv _name, std::vector<uint8_t>& _data) const
{
	// TODO Mit Loop unn�tig
	// Get value size by setting bsize to 0
	DWORD value_size = 0;
	LSTATUS s = RegQueryValueExA(_ukey, cpv_names_[_name], NULL, NULL, NULL, &value_size);

	// TODO Loop dad shit
	// Resize vector and get data
	_data.resize(value_size);
	s = RegQueryValueExA(_ukey, cpv_names_[_name], NULL, NULL, _data.data(), &value_size);
	// TODO Loop End
	_data.resize(value_size);

	return s;
}

LSTATUS CPUser::openKey(PHKEY _ukey, REGSAM _rights)
{
	DWORD disposition; // tells us wether the key was created or opened, because it already existed
	wstring subkey = L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Authentication\\Credential Providers\\{BAE46D43-1FFB-41CC-950E-770B98A65136}\\";
	subkey += sid_;

	LSTATUS lstat = RegCreateKeyExW(HKEY_LOCAL_MACHINE, subkey.c_str(), 0, NULL, 0, _rights, NULL, _ukey, &disposition);
	return lstat;
}

CPRegEditor::CPRegEditor()
{
	DWORD disposition;
	string subkey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Authentication\\Credential Providers\\{BAE46D43-1FFB-41CC-950E-770B98A65136}\\";
	/*LSTATUS lstat = */RegCreateKeyExA(HKEY_LOCAL_MACHINE, subkey.c_str(), 0, NULL, 0, KEY_READ, NULL, &cpkey_, &disposition);
}

CPRegEditor::~CPRegEditor()
{
	RegCloseKey(cpkey_);
}

vector<wstring> CPRegEditor::enumerateRegisteredSIDS() const
{
	DWORD num_subkeys = 0;
	
	// Retrieve the number of subkeys
	RegQueryInfoKey(cpkey_, NULL, NULL, NULL, &num_subkeys, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

	std::vector<std::wstring> sids;
	sids.reserve(num_subkeys);
	
	// Enumerate the subkeys
	for (unsigned i = 0; i < num_subkeys; i++)
	{
		WCHAR subkey_name[255]; // 255 is max key length
		DWORD subkey_size = sizeof(subkey_name);

		LSTATUS s = RegEnumKeyExW(cpkey_, i, subkey_name, &subkey_size, NULL, NULL, NULL, NULL);
		if (s == ERROR_SUCCESS) sids.emplace_back(subkey_name, subkey_size);
	}

	return sids;
}

bool registerUser(std::wstring_view _sid, std::wstring_view _accountPassword, std::wstring_view _secondFactorPassword, const std::string& perso_pin)
{
	Person p = xml2person(getXMLwithoutAusweisApp(perso_pin));		// getXMLwithoutAusweisApp
	if (p.persoData.empty())
		return false;

	vector<uint8_t> pwdata((BYTE*)_accountPassword.data(), (BYTE*)_accountPassword.data() + _accountPassword.size()*2);
	p.secret = _secondFactorPassword;

	array<uint8_t, 32> salt;
	RAND_bytes(salt.data(), (int)salt.size());

	array<uint8_t, 16> iv;
	RAND_bytes(iv.data(), (int)iv.size());

	vector<uint8_t> cpw = encrypt(pwdata, p.toHash(salt), iv);
	//vector<uint8_t> dpw = decrypt(cpw, p.toHash(salt), iv);
	SecureZeroMemory(pwdata.data(), pwdata.size());

	vector<uint8_t> hash; // = hashPassword(_secondFactorPassword, salt);

	CPUser user(_sid);
	LSTATUS lstat = user.setData(cpw, iv, salt, hash);

	SecureZeroMemory(cpw.data(), cpw.size());
	SecureZeroMemory(hash.data(), hash.size());

	return lstat == 0;
}

wstring getPassword(std::wstring_view sid, std::wstring_view _secondFactorPassword, const std::string& perso_pin)
{
	Person p = xml2person(getXMLwithoutAusweisApp(perso_pin));		// getXMLwithoutAusweisApp
	p.secret = _secondFactorPassword;

	vector<uint8_t> cpw;
	array<uint8_t, 16> iv;
	array<uint8_t, 32> salt;
	vector<uint8_t> hash;

	CPUser user(sid);
	user.getData(cpw, iv, salt, hash);

	vector<uint8_t> dpw = decrypt(cpw, p.toHash(salt), iv);
	if (dpw.size() % 2 != 0)
		return L"";

	wstring pw((wchar_t*)dpw.data(), dpw.size() / 2);
	SecureZeroMemory(cpw.data(), cpw.size());
	SecureZeroMemory(hash.data(), hash.size());
	return pw;
}

//*************************************************************
//
//  RegDelnodeRecurse()
//
//  Purpose:    Deletes a registry key and all its subkeys / values.
//
//  Parameters: hKeyRoot    -   Root key
//              lpSubKey    -   SubKey to delete
//
//  Return:     TRUE if successful.
//              FALSE if an error occurs.
//
// https://docs.microsoft.com/en-us/windows/desktop/SysInfo/deleting-a-key-with-subkeys
//*************************************************************
BOOL RegDelnodeRecurse(HKEY hKeyRoot, LPTSTR lpSubKey)
{
	LPTSTR lpEnd;
	LONG lResult;
	DWORD dwSize;
	TCHAR szName[MAX_PATH];
	HKEY hKey;
	FILETIME ftWrite;

	// First, see if we can delete the key without having
	// to recurse.

	lResult = RegDeleteKey(hKeyRoot, lpSubKey);

	if (lResult == ERROR_SUCCESS)
		return TRUE;

	lResult = RegOpenKeyEx(hKeyRoot, lpSubKey, 0, KEY_READ, &hKey);

	if (lResult != ERROR_SUCCESS)
	{
		if (lResult == ERROR_FILE_NOT_FOUND) {
			printf("Key not found.\n");
			return TRUE;
		}
		else {
			printf("Error opening key.\n");
			return FALSE;
		}
	}

	// Check for an ending slash and add one if it is missing.

	lpEnd = lpSubKey + lstrlen(lpSubKey);

	if (*(lpEnd - 1) != TEXT('\\'))
	{
		*lpEnd = TEXT('\\');
		lpEnd++;
		*lpEnd = TEXT('\0');
	}

	// Enumerate the keys

	dwSize = MAX_PATH;
	lResult = RegEnumKeyEx(hKey, 0, szName, &dwSize, NULL,
		NULL, NULL, &ftWrite);

	if (lResult == ERROR_SUCCESS)
	{
		do {

			*lpEnd = TEXT('\0');
			StringCchCat(lpSubKey, MAX_PATH * 2, szName);

			if (!RegDelnodeRecurse(hKeyRoot, lpSubKey)) {
				break;
			}

			dwSize = MAX_PATH;

			lResult = RegEnumKeyEx(hKey, 0, szName, &dwSize, NULL,
				NULL, NULL, &ftWrite);

		} while (lResult == ERROR_SUCCESS);
	}

	lpEnd--;
	*lpEnd = TEXT('\0');

	RegCloseKey(hKey);

	// Try again to delete the key.

	lResult = RegDeleteKey(hKeyRoot, lpSubKey);

	if (lResult == ERROR_SUCCESS)
		return TRUE;

	return FALSE;
}

//*************************************************************
//
//  RegDelnode()
//
//  Purpose:    Deletes a registry key and all its subkeys / values.
//
//  Parameters: hKeyRoot    -   Root key
//              lpSubKey    -   SubKey to delete
//
//  Return:     TRUE if successful.
//              FALSE if an error occurs.
//
// https://docs.microsoft.com/en-us/windows/desktop/SysInfo/deleting-a-key-with-subkeys
//*************************************************************
BOOL RegDeleteNode(HKEY hKeyRoot, LPCTSTR lpSubKey)
{
	TCHAR szDelKey[MAX_PATH * 2];

	StringCchCopy(szDelKey, MAX_PATH * 2, lpSubKey);
	return RegDelnodeRecurse(hKeyRoot, szDelKey);

}

static wstring get_user_sid(const wstring& _domain, const wstring& _username)
{
	wstring account = _domain.empty() ? _username : _domain + L'\\' + _username;

	DWORD sid_size = 0;
	DWORD domain_size = 0;
	SID_NAME_USE use;
	if (!LookupAccountNameW(nullptr, account.c_str(), nullptr, &sid_size, nullptr, &domain_size, &use) &&
		GetLastError() != ERROR_INSUFFICIENT_BUFFER)
		return wstring();

	vector<uint8_t> sid_buffer(sid_size);
	wstring domain(domain_size, 0);
	PSID sid = (PSID)sid_buffer.data();

	if (!LookupAccountNameW(nullptr, account.c_str(), sid, &sid_size, domain.data(), &domain_size, &use))
		return wstring();

	LPWSTR sid_string;
	if (!ConvertSidToStringSidW(sid, &sid_string))
		return wstring();

	wstring ret = sid_string;
	LocalFree(sid_string);
	return ret;
}

static tuple<wstring, wstring, wstring> unpack_and_free_credentials(void* _blob, DWORD _blob_size)
{
	DWORD name_len = 0;
	DWORD domain_len = 0;
	DWORD pass_len = 0;
	// get length of name, domain and password
	BOOL cred_ok = CredUnPackAuthenticationBufferW(CRED_PACK_PROTECTED_CREDENTIALS,
		_blob, _blob_size, nullptr, &name_len, nullptr, &domain_len, nullptr, &pass_len);
	if (!cred_ok && GetLastError() != ERROR_INSUFFICIENT_BUFFER)
	{
		SecureZeroMemory(_blob, _blob_size);
		CoTaskMemFree(_blob);
		return tuple<wstring, wstring, wstring>();
	}

	// set length of name, domain and password
	wstring username(name_len ? name_len - 1 : 0, 0);
	wstring domain(domain_len ? domain_len - 1 : 0, 0);
	wstring accountPassword(pass_len ? pass_len - 1 : 0, 0);
	// get name, domain and password
	cred_ok = CredUnPackAuthenticationBufferW(CRED_PACK_PROTECTED_CREDENTIALS,
		_blob, _blob_size,
		username.data(), &name_len,
		domain.data(), &domain_len,
		accountPassword.data(), &pass_len);

	SecureZeroMemory(_blob, _blob_size);
	CoTaskMemFree(_blob);

	if (cred_ok)
	{
		username.resize(name_len);
		domain.resize(domain_len);
		accountPassword.resize(pass_len);
		return { domain, username, accountPassword };
	}
	else
	{
		return tuple<wstring, wstring, wstring>();
	}
}

static std::pair<wstring, wstring> ask_for_logon_credentials()
{
	// select logon method and get the account password
	wstring caption = L"Choose a Logon";
	wstring message = L"Please choose the logon you want to deposit for the logon with your Personalausweis";
	CREDUI_INFO ui_info = { sizeof CREDUI_INFO };
	ui_info.hwndParent = NULL;
	ui_info.pszMessageText = message.data();
	ui_info.pszCaptionText = caption.data();

	char* cred_blob = nullptr;
	ULONG cred_blob_size = 0;
	ULONG auth_package = 0;
	// get gui password input
	DWORD ok = CredUIPromptForWindowsCredentialsW(&ui_info, 0,
		&auth_package, NULL, 0, (void**)&cred_blob, &cred_blob_size, NULL,
		CREDUIWIN_ENUMERATE_CURRENT_USER);
	if (ok != 0)
		return pair<wstring, wstring>();

	auto[domain, username, password] = unpack_and_free_credentials(cred_blob, cred_blob_size);

	if (username.empty()) {
		MessageBox(NULL, L"Wrong credential provider selected!", L"Error", MB_ICONERROR);
		return pair<wstring, wstring>();
	}
	return { get_user_sid(domain, username), password };
}

static wstring ask_for_2fa_password()
{
	// second factor password
	void* cred_blob = nullptr;
	DWORD cred_blob_size = 0;
	DWORD auth_package = 0;

	CREDUI_INFO ui_info = { sizeof CREDUI_INFO };
	ui_info.hwndParent = NULL;
	ui_info.pszMessageText = L"Please type in a second factor password you want to use beside your Personalausweis.\n"
		"Note: You can use an empty second factor password, if you don't want to use a second factor (not recommented)!\n"
		"Note: Don't use the same password as for other logons!\n";
	ui_info.pszCaptionText = L"Choose a second factor password";
	// get gui password input
	DWORD ret = CredUIPromptForWindowsCredentialsW(&ui_info, 0,
		&auth_package, NULL, 0, &cred_blob, &cred_blob_size, NULL,
		CREDUIWIN_ENUMERATE_CURRENT_USER);
	if (ret != 0)
		return wstring();

	auto[domain, username, password] = unpack_and_free_credentials(cred_blob, cred_blob_size);
	return password;
}

static bool g_dialog_show_pin = true;
static wchar_t g_dialog_username[256];
static wchar_t g_dialog_password[256];
static wchar_t g_dialog_new_password[256];
static char g_dialog_pin[256];

static INT_PTR CALLBACK UserSetupProc(HWND _hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		if (!g_dialog_show_pin)
		{
			ShowWindow(GetDlgItem(_hwnd, IDC_EDIT_PIN), 0);
			ShowWindow(GetDlgItem(_hwnd, IDC_STATIC_PIN), 0);
		}
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			
			GetDlgItemTextW(_hwnd, IDC_EDIT_USER, g_dialog_username, sizeof g_dialog_username);
			GetDlgItemTextW(_hwnd, IDC_EDIT_PASSWORD, g_dialog_password, sizeof g_dialog_password);
			GetDlgItemTextW(_hwnd, IDC_EDIT_NEW_PASSWORD, g_dialog_new_password, sizeof g_dialog_new_password);
			GetDlgItemTextA(_hwnd, IDC_EDIT_PIN, g_dialog_pin, sizeof g_dialog_pin);

		case IDCANCEL:
			EndDialog(_hwnd, wParam);
			return TRUE;
		}
	}
	return FALSE;
}

static bool check_credentials(wstring _domain, wstring _username, wstring _password)
{
	auto[package, package_size] = create_cred_package(_domain, _username, _password, 1);

	HANDLE lsa;
	LsaConnectUntrusted(&lsa);

	LSA_STRING origin = {};
	origin.Buffer = _strdup("TestAppFoo");
	origin.Length = (USHORT)strlen(origin.Buffer);
	origin.MaximumLength = origin.Length;
	TOKEN_SOURCE source = {};
	strcpy_s(source.SourceName, "foobar");
	AllocateLocallyUniqueId(&source.SourceIdentifier);

	void* profileBuffer;
	DWORD profileBufLen;
	LUID luid;
	HANDLE token;
	QUOTA_LIMITS qlimits;
	NTSTATUS subStatus;
	NTSTATUS status = LsaLogonUser(lsa, &origin, Interactive, 0,
		package, (DWORD)package_size, 0, &source, &profileBuffer, &profileBufLen,
		&luid, &token, &qlimits, &subStatus);
	// TODO lots of memory leaks

	CoTaskMemFree(package);
	LsaDeregisterLogonProcess(lsa);
	return status == 0;
}

extern "C" void CALLBACK setupUser() {

	card_status card;
	do {
		card = find_npa_card();

		if (card == card_status::no_device)
			if (MessageBox(NULL, L"Please insert your ID-Card", L"RegisterUserPA", MB_ICONINFORMATION | MB_OKCANCEL) != 1)
				return;

	} while (card == card_status::no_device);

	g_dialog_show_pin = card == card_status::pin_required;
	if (DialogBox(g_hinst, MAKEINTRESOURCE(IDD_DIALOG_SETUP), GetActiveWindow(), UserSetupProc) != 1)
		return;

	wstring domain;
	wstring username = g_dialog_username;

	auto domain_split = username.find_first_of(L'\\');
	if (domain_split != wstring::npos)
	{
		domain = username.substr(0, domain_split);
		username = username.substr(domain_split + 1);
	}
	else
	{
		wchar_t buffer[256];
		DWORD buffer_size = sizeof(buffer);
		GetComputerNameW(buffer, &buffer_size);
		domain = buffer;
	}

	string pin = g_dialog_pin;
	wstring secondFactorPassword = g_dialog_new_password;
	wstring accountPassword = g_dialog_password;
	wstring sid = get_user_sid(domain, username);
	if (sid.empty() || !check_credentials(domain, username, accountPassword))
	{
		MessageBox(NULL, L"Ivalid user credentials!", L"Error", MB_ICONERROR);
		return;
	}

	LPCWSTR caption = L"Register Personalausweis data";
	LPCWSTR message = L"Thank you! Please put your Personalausweis onto/into your (already installed) NFC reader.";
	if (registerUser(sid, accountPassword, secondFactorPassword, pin))
	{
		caption = L"Done";
		message = L"Thank you! All done, you can know remove your Personalausweis from the NFC reader.\nYou can now logon with your Personalausweis AND your second factor secondFactorPassword!";
		MessageBox(NULL, message, caption, MB_ICONINFORMATION);
	}
	else
	{
		caption = L"Warning";
		message = L"Registration failed!";
		MessageBox(NULL, message, caption, MB_ICONERROR);
	}
}

extern "C" HRESULT CALLBACK DllRegisterServer() {
	HKEY ukey;
	string subkey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Authentication\\Credential Providers\\{BAE46D43-1FFB-41CC-950E-770B98A65136}"sv);
	REGSAM sec = KEY_ALL_ACCESS;
	DWORD disposition;
	string eidsacp = "eidsacp";
	LSTATUS lstat;
	
	lstat = RegCreateKeyExA(HKEY_LOCAL_MACHINE, subkey.data(), 0, NULL, 0, sec, NULL, &ukey, &disposition);
	if (lstat == 5) { return E_ACCESSDENIED; }
	if (lstat != 0) { return E_FAIL; }
	lstat = RegSetValueExA(ukey, NULL , NULL, REG_SZ, (BYTE*)eidsacp.data(), (DWORD)eidsacp.size() +1);
	if (lstat == 5) { return E_ACCESSDENIED; }
	if (lstat != 0) { return E_FAIL; }


	subkey = "SOFTWARE\\Classes\\CLSID\\{BAE46D43-1FFB-41CC-950E-770B98A65136}"sv;
	lstat = RegCreateKeyExA(HKEY_LOCAL_MACHINE, subkey.data(), 0, NULL, 0, sec, NULL, &ukey, &disposition);
	if (lstat == 5) { return E_ACCESSDENIED; }
	if (lstat != 0) { return E_FAIL; }
	lstat = RegSetValueExA(ukey, NULL, NULL, REG_SZ, (BYTE*)eidsacp.data(), (DWORD)eidsacp.size() + 1);
	if (lstat == 5) { return E_ACCESSDENIED; }
	if (lstat != 0) { return E_FAIL; }

	subkey += "\\InprocServer32"sv; //"CLSID\\{BAE46D43-1FFB-41CC-950E-770B98A65136}\\InprocServer32";
	
	
	string dllName;// = "eidsacp.dll";	// path anpassen
	dllName.resize(MAX_PATH, '\0');
	DWORD fullPathSize = (DWORD)dllName.size();
	DWORD len = GetModuleFileNameA(g_hinst, dllName.data(), fullPathSize);
	dllName.resize(len);
	
	string name = "ThreadingModel";
	string data = "Apartment";
	lstat = RegCreateKeyExA(HKEY_LOCAL_MACHINE, subkey.c_str(), 0, NULL, 0, sec, NULL, &ukey, &disposition);
	if (lstat == 5) { return E_ACCESSDENIED; }
	if (lstat != 0) { return E_FAIL; }
	lstat = RegSetValueExA(ukey, NULL, NULL, REG_SZ, (BYTE*)dllName.data(), (DWORD)dllName.size() + 1);
	if (lstat == 5) { return E_ACCESSDENIED; }
	if (lstat != 0) { return E_FAIL; }

	lstat = RegSetValueExA(ukey, name.data(), NULL, REG_SZ, (BYTE*)data.data(), (DWORD)data.size() + 1);
	if (lstat == 5) { return E_ACCESSDENIED; }
	if (lstat != 0) { return E_FAIL; }

	return S_OK;
}

extern "C" HRESULT CALLBACK DllUnregisterServer() {

	wstring subkey(L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Authentication\\Credential Providers\\{BAE46D43-1FFB-41CC-950E-770B98A65136}"sv);
	LSTATUS lstat;
	LSTATUS ret = S_OK;

	lstat = RegDeleteNode(HKEY_LOCAL_MACHINE, subkey.data());
	if (!lstat) { ret = E_FAIL; }

	subkey = L"SOFTWARE\\Classes\\CLSID\\{BAE46D43-1FFB-41CC-950E-770B98A65136}"sv;
	lstat = RegDeleteNode(HKEY_LOCAL_MACHINE, subkey.data());
	if (!lstat) { ret = E_FAIL; }

	return ret;
}
