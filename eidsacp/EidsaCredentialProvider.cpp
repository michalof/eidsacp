#include "dllmain.h"
#include "EidsaCredentialProvider.h"
#include "EidsaCredentialFields.h"
#include "helper.h"
#include "registerUser.h"
#include <algorithm>
#include <ShlGuid.h>

EidsaCredentialField g_fields[ECFI_NUM_FIELDS] =
{
	{ { ECFI_TILEIMAGE, CPFT_TILE_IMAGE, (LPWSTR)L"IMAGE", CPFG_CREDENTIAL_PROVIDER_LOGO }, CPFS_DISPLAY_IN_BOTH, CPFIS_NONE },
	{ { ECFI_LABEL, CPFT_SMALL_TEXT, (LPWSTR)L"Logon mit Personalausweis (SA)", CPFG_CREDENTIAL_PROVIDER_LABEL }, CPFS_DISPLAY_IN_DESELECTED_TILE, CPFIS_NONE },
	{ { ECFI_PASSWORD, CPFT_PASSWORD_TEXT, (LPWSTR)L"Password", CPFG_LOGON_PASSWORD }, CPFS_DISPLAY_IN_SELECTED_TILE, CPFIS_FOCUSED },
	{ { ECFI_PIN, CPFT_PASSWORD_TEXT, (LPWSTR)L"PIN" }, CPFS_DISPLAY_IN_SELECTED_TILE, CPFIS_NONE },
	{ { ECFI_SUBMIT, CPFT_SUBMIT_BUTTON, (LPWSTR)L"SUBMIT" }, CPFS_DISPLAY_IN_SELECTED_TILE, CPFIS_NONE },
};

EidsaCredentialProvider::EidsaCredentialProvider()
{
	DllAddRef();
}

EidsaCredentialProvider::~EidsaCredentialProvider()
{
	DllRelease();
	for (EidsaCredential* credential : credentials_)
		credential->Release();
}

HRESULT EidsaCredentialProvider::SetUsageScenario(CREDENTIAL_PROVIDER_USAGE_SCENARIO cpus, DWORD dwFlags)
{
	HRESULT hres;

	switch (cpus)
	{
	case CPUS_LOGON:
	case CPUS_UNLOCK_WORKSTATION:
	case CPUS_CREDUI:
		cpus_ = cpus;
		hres = S_OK;
		break;

	case CPUS_CHANGE_PASSWORD:
	case CPUS_PLAP:
		hres = E_NOTIMPL;
		break;

	default:
		hres = E_INVALIDARG;
		break;
	}

	return hres;
}

HRESULT EidsaCredentialProvider::SetSerialization(const CREDENTIAL_PROVIDER_CREDENTIAL_SERIALIZATION * pcpcs)
{
	return E_NOTIMPL;
}

HRESULT EidsaCredentialProvider::Advise(ICredentialProviderEvents * pcpe, UINT_PTR upAdviseContext)
{
	return E_NOTIMPL;
}

HRESULT EidsaCredentialProvider::UnAdvise()
{
	return E_NOTIMPL;
}

HRESULT EidsaCredentialProvider::GetFieldDescriptorCount(DWORD * pdwCount)
{
	*pdwCount = ECFI_NUM_FIELDS;
	return S_OK;
}

HRESULT EidsaCredentialProvider::GetFieldDescriptorAt(DWORD dwIndex, CREDENTIAL_PROVIDER_FIELD_DESCRIPTOR ** ppcpfd)
{
	if (dwIndex >= ECFI_NUM_FIELDS || !ppcpfd) return E_INVALIDARG;
	*ppcpfd = nullptr;
	return FieldDescriptorCoAllocCopy(g_fields[dwIndex].descriptor, ppcpfd);
}

HRESULT EidsaCredentialProvider::GetCredentialCount(DWORD * pdwCount, DWORD * pdwDefault, BOOL * pbAutoLogonWithDefault)
{
	// TODO validate assumptions:
	// I also expect, that SetUsageScenario was already called and users_ contains only one element, if the scenario is UNLOCK_WORKSTATION

	if (users_)
	{
		setupCredentials();

		*pdwDefault = CREDENTIAL_PROVIDER_NO_DEFAULT;
		*pbAutoLogonWithDefault = FALSE;
		*pdwCount = (DWORD)credentials_.size();
		

		return S_OK;
	}
	else
	{
		return E_UNEXPECTED;
	}
}

HRESULT EidsaCredentialProvider::GetCredentialAt(DWORD dwIndex, ICredentialProviderCredential ** ppcpc)
{
	if (dwIndex >= credentials_.size() || !ppcpc) return E_INVALIDARG;

	*ppcpc = credentials_[dwIndex];
	credentials_[dwIndex]->AddRef();
	return S_OK;
}

HRESULT EidsaCredentialProvider::SetUserArray(ICredentialProviderUserArray * users)
{
	if (users_) users_->Release();
	users_ = users;
	users_->AddRef();
	return S_OK;
}

void EidsaCredentialProvider::setupCredentials()
{
	for (EidsaCredential* credential : credentials_)
		credential->Release();
	credentials_.clear();

	std::vector<std::wstring> registered_users = CPRegEditor().enumerateRegisteredSIDS();
	std::vector<std::wstring> logon_users;
	DWORD userCount = 0;
	users_->GetCount(&userCount);

	for (DWORD i = 0; i < userCount; ++i) {
		ICredentialProviderUser *pCredUser;
		users_->GetAt(i, &pCredUser);
		LPWSTR s;
		pCredUser->GetSid(&s);
		logon_users.emplace_back(s);
		CoTaskMemFree(s);
		pCredUser->Release();
	}
	std::sort(registered_users.begin(), registered_users.end());
	std::sort(logon_users.begin(), logon_users.end());

	std::vector<std::wstring> users;

	std::set_intersection(registered_users.begin(), registered_users.end(), logon_users.begin(), logon_users.end(), std::back_inserter(users));

	for (std::wstring& sid : users) {
		credentials_.push_back(new EidsaCredential(std::move(sid), cpus_));
	}
}
