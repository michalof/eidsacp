#pragma once
#include <credentialprovider.h>

enum EidsaCredentialFieldID
{
	ECFI_TILEIMAGE,
	ECFI_LABEL,
	ECFI_PASSWORD,
	ECFI_PIN,
	ECFI_SUBMIT,
	ECFI_NUM_FIELDS
};

struct EidsaCredentialField
{
	CREDENTIAL_PROVIDER_FIELD_DESCRIPTOR descriptor; // unique field ID + field type + label + special field type (optional)
	CREDENTIAL_PROVIDER_FIELD_STATE display; // When should this field be displayed? (When the tile is selected/deselected/both/never)
	CREDENTIAL_PROVIDER_FIELD_INTERACTIVE_STATE istate; // editable/readonly/disabled/focused
};

extern EidsaCredentialField g_fields[ECFI_NUM_FIELDS];
