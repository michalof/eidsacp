README for eidsacp (eID Selbstauskunft Credential Provider)

Install:
1. Move all files from eidsacp.zip to the directory of your choice (the directory should be only writeable for administrator e.g. program files).
2. Register the new Credential Provider:
	- Change into the directory of the dll-files.
	- Open cmd there with administrator rights.
	- Type in: RegSvr32 eidsacp.dll



Setup a new eidsacp for an user:
1. Logon with the user you want to setup eidsacp for.
2. Install all drivers for your nfc reader.
3. Run: RegisterUserPA.exe and follow the instructions!
NOTE: RegisterUserPA.exe will ask for admin rigths, because the credentials will be stored in the registry!
4. Congratualtion, you are done! You can now logon with your Personalausweis onto/into the nfc reader, the pin of your Personalausweis and your choosen second factor password.


Remove eidsacp for an user:
1. Get the sid of the user.
2. Open the Registry:
	- Run: regedit
3. Delete the subkey named with the users sid you want to remove in the following path:
	HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Authentication\Credential Providers\BAE46D43-1FFB-41CC-950E-770B98A65136



Unistall (This will delete all stored credentials for all users!):
1. Unregister the Credential Provider:
	- Change into the directory of the dll-files.
	- Open cmd there with administrator rights.
	- Type in: RegSvr32 /u eidsacp.dll
2. Remove all dll-files of the directory of your choice.
