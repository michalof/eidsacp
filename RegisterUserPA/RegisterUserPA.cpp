#include <Windows.h>

int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow
)
{
	HMODULE setup = LoadLibraryW(L"eidsacp.dll");
	if (!setup)
	{
		MessageBoxW(NULL, L"Failed to load eidsacp.dll.", L"RegisterUserPA", MB_ICONERROR);
		return 1;
	}

	using fct_setUser = void(void);

	fct_setUser * setupUser = (fct_setUser *)GetProcAddress(setup, "setupUser");
	if (setupUser == 0) {
		FreeLibrary(setup);
		return 1;
	}

	setupUser();
	FreeLibrary(setup);



	return 0;
}
